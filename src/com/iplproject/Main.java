package com.iplproject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    private final static int ID = 0;
    private final static int SEASON = 1;
    private final static int WINNER = 10;
    private final static int MATCH_ID = 0;
    private final static int BOWLING_TEAM = 3;
    private final static int BATSMAN = 6;
    private final static int BOWLER = 8;
    private final static int BATSMAN_RUNS = 15;
    private final static int EXTRA_RUNS = 16;
    private final static int TOTAL_RUNS = 17;
    private final static int PLAYER_DISMISSED = 18;
    private final static int DISMISSAL_KIND =19;


    public static void main(String[] args) {
        List<Match> matches = matchesData("src/matches.csv");
        List<Delivery> deliveries = deliveriesData("src/deliveries.csv");;

        getMatchesPlayedPerYear(matches);
        getMatchesWonPerTeamInPerYear(matches);
        getExtraRunsConcededPerTeam(matches,deliveries);
        getTopEconomicalBowlersIn2015(matches,deliveries);
        getCountHowManyTimesParticularPlayerOutByCaughtIn2010(matches,deliveries);
        findOverTakenByJjBumrahIn2016(matches,deliveries);
        getNumberOfBoundaryHitByKohliIn2017(matches,deliveries);
        findRunScoredByChGayleInEachSeason(matches,deliveries);
        findPlayersDismissedByDjHooda(deliveries);
        findPlayersWhoOutByStumped(deliveries);
    }

    public static List<Match> matchesData(String  matchesDataPath){
        List<Match> matches = new ArrayList<>();

        try {
            BufferedReader matchesBr = new BufferedReader(new FileReader(matchesDataPath));
            matchesBr.readLine();
            String line = "";
            while ((line = matchesBr.readLine()) != null){
                Match match = new Match();
                String[] matchData = line.split(",");
                match.setId(Integer.parseInt(matchData[ID]));
                match.setSeason(Integer.parseInt(matchData[SEASON]));
                match.setWinner(matchData[WINNER]);
                matches.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matches;
    }

    public static List<Delivery> deliveriesData(String deliveriesDataPath){
        List<Delivery> deliveries = new ArrayList<>();

        try {
            BufferedReader deliveriesBr = new BufferedReader(new FileReader(deliveriesDataPath));
            deliveriesBr.readLine();
            String line = "";

            while ((line = deliveriesBr.readLine()) != null){
                Delivery delivery = new Delivery();
                String[] deliveryData = line.split(",");
                delivery.setMatchId(Integer.parseInt(deliveryData[MATCH_ID]));
                delivery.setBowlingTeam(deliveryData[BOWLING_TEAM]);
                delivery.setBatsman(deliveryData[BATSMAN]);
                delivery.setBowler(deliveryData[BOWLER]);
                delivery.setBatsmanRuns(Integer.parseInt(deliveryData[BATSMAN_RUNS]));
                delivery.setExtraRuns(Integer.parseInt(deliveryData[EXTRA_RUNS]));
                delivery.setTotalRuns(Integer.parseInt(deliveryData[TOTAL_RUNS]));
                if(deliveryData.length == 21){
                    delivery.setPlayerDismissed(deliveryData[PLAYER_DISMISSED]);
                    delivery.setDismissalKind(deliveryData[DISMISSAL_KIND]);
                }
                else {
                    delivery.setPlayerDismissed("");
                    delivery.setDismissalKind("");
                }
                deliveries.add(delivery);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    public static Map<Integer,Integer> getMatchesPlayedPerYear(List<Match>  matches)  {
        Map<Integer,Integer> matchesPlayedPerYear = new HashMap<Integer,Integer>();
        int matchSeason = 0;

        for (Match match: matches ) {
            matchSeason = match.getSeason();
            if(matchesPlayedPerYear.containsKey(matchSeason)){
                matchesPlayedPerYear.put(matchSeason,(matchesPlayedPerYear.get(matchSeason)+1));
            }
            else {
                matchesPlayedPerYear.put(matchSeason,1);
            }
        }
        System.out.println("\nNumber of matches played per year of all the years in IPL");
        System.out.println(matchesPlayedPerYear+"\n");

        return matchesPlayedPerYear;
    }

    public static Map<String,Integer> getMatchesWonPerTeamInPerYear(List<Match> matches){
        Map<String,Integer> matchesWonPerTeamInPerYear = new HashMap<String,Integer>();
        String winner="";

        for ( Match match : matches) {
            winner  = match.getWinner();
            if(!winner.isEmpty()){
                if(matchesWonPerTeamInPerYear.containsKey(winner)){
                    matchesWonPerTeamInPerYear.put(winner,(matchesWonPerTeamInPerYear.get(winner)+1));
                }
                else {
                    matchesWonPerTeamInPerYear.put(winner,1);
                }
            }
        }
        System.out.println("\nNumber of matches won of all teams over all the years of IPL");
        System.out.println(matchesWonPerTeamInPerYear+"\n");
        return  matchesWonPerTeamInPerYear;
    }

    static Map<String,Integer> getExtraRunsConcededPerTeam(List<Match> matches, List<Delivery> deliveries){
        int matchId = 0;
        int matchSeason = 0;
        int deliveryId = 0;
        String  bowlingTeam = "";
        int extraRuns = 0;
        List<Integer> matchIdsOfYear2016 = new ArrayList<Integer>() ;
        Map<String, Integer> extraRunConcededPerTeamIn2016 = new HashMap<>();

        for ( Match match: matches) {
            matchId = match.getId();
            matchSeason = match.getSeason();
            if(!matchIdsOfYear2016.contains(matchId) && matchSeason == 2016){
                matchIdsOfYear2016.add(matchId);
            }
        }
        for ( Delivery delivery : deliveries ) {
            deliveryId = delivery.getMatchId();
            bowlingTeam = delivery.getBowlingTeam();
            extraRuns = delivery.getExtraRuns();
            if(matchIdsOfYear2016.contains(deliveryId)){
                //System.out.println(extraRuns);
                if(extraRunConcededPerTeamIn2016.containsKey(bowlingTeam)){
                    extraRunConcededPerTeamIn2016.put(bowlingTeam,(extraRunConcededPerTeamIn2016.get(bowlingTeam))+extraRuns);
                }
                else {
                    extraRunConcededPerTeamIn2016.put( bowlingTeam, extraRuns);
                }
            }
        }
        System.out.println("\nFor the year 2016 get the extra runs conceded per team.");
        System.out.println(extraRunConcededPerTeamIn2016+"\n");
        return extraRunConcededPerTeamIn2016;
    }

    static Map<String,Float> getTopEconomicalBowlersIn2015(List<Match> matches, List<Delivery> deliveries){
        int matchId = 0;
        int matchSeason = 0;
        int deliveryId = 0;
        int extraRuns = 0;
        String bowler = "";
        int totalRuns = 0;
        int totalRun;
        int totalBall;
        float economy;
        List<Integer> matchIdsOfYear2015 = new ArrayList<Integer>() ;
        Map<String, List<Integer>> totalRunAndBalls = new HashMap<>();
        HashMap<String,Float> bowlersEconomy= new HashMap<String,Float>();
        Map<String, Float> topEconomicalBowler = new TreeMap<>();

        for ( Match match: matches) {
            matchId = match.getId();
            matchSeason = match.getSeason();
            if(!matchIdsOfYear2015.contains(matchId) && matchSeason == 2015){
                matchIdsOfYear2015.add(matchId);
            }
        }
        for ( Delivery delivery: deliveries) {
            deliveryId = delivery.getMatchId();
            extraRuns = delivery.getExtraRuns();
            bowler = delivery.getBowler();
            totalRuns = delivery.getTotalRuns();

            if (matchIdsOfYear2015.contains(deliveryId)) {
                if (extraRuns == 0) {
                    if (totalRunAndBalls.containsKey(bowler)) {
                        totalRunAndBalls.get(bowler).set(0, totalRunAndBalls.get(bowler).get(0) + totalRuns);
                        totalRunAndBalls.get(bowler).set(1, totalRunAndBalls.get(bowler).get(1) + 1);
                    } else {
                        totalRunAndBalls.put(bowler, new ArrayList<Integer>());
                        totalRunAndBalls.get(bowler).add(0, totalRuns);
                        totalRunAndBalls.get(bowler).add(1, 1);
                    }
                }
            }
        }
        for ( String key : totalRunAndBalls.keySet() ) {
            totalRun =  totalRunAndBalls.get(key).get(0);
            totalBall =  totalRunAndBalls.get(key).get(1);
            economy = totalRun/(totalBall/6f);
            bowlersEconomy.put(key,economy);
        }
        topEconomicalBowler = sortByValue(bowlersEconomy);
        System.out.println("\nTop economical bawler in 2015");
        System.out.println(topEconomicalBowler+"\n");
        return topEconomicalBowler;
    }

    public static HashMap<String, Float> sortByValue(HashMap<String, Float> hashMap)
    {
        List<Map.Entry<String, Float> > list = new LinkedList<Map.Entry<String, Float> >(hashMap.entrySet());
        System.out.println("list "+list);
        Collections.sort(list, new Comparator<Map.Entry<String, Float> >() {
            public int compare(Map.Entry<String, Float> obj1, Map.Entry<String, Float> obj2) {
                return (obj1.getValue()).compareTo(obj2.getValue());
            }
        });
        HashMap<String, Float> sortedHasMap = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> aa : list) {
            sortedHasMap.put(aa.getKey(), aa.getValue());
        }
        return sortedHasMap;
    }

    static Map<String, Integer> getCountHowManyTimesParticularPlayerOutByCaughtIn2010(List<Match> matches, List<Delivery> deliveries){
        int matchId = 0;
        int matchSeason = 0;
        int deliveryId = 0;
        String dismissalKind = "";
        String batsman = "";
        Map<String, Integer> playersOutByCaughtIn2010 = new HashMap<String, Integer>();
        List<Integer> matchIdsOfYear2010 = new ArrayList<Integer>() ;

        for (Match match: matches) {
            matchId = match.getId();
            matchSeason = match.getSeason();

            if(!matchIdsOfYear2010.contains(matchId) && matchSeason == 2010){
                matchIdsOfYear2010.add(matchId);
            }
        }
        for( Delivery delivery : deliveries ) {
           deliveryId = delivery.getMatchId();
           dismissalKind = delivery.getDismissalKind();
           batsman = delivery.getBatsman();
           if(matchIdsOfYear2010.contains(deliveryId)&& !dismissalKind.equals("")) {
               if (dismissalKind.equals("caught")) {
                    //System.out.println(deliveriesData[6]);
                    if (playersOutByCaughtIn2010.containsKey(batsman)) {
                        // System.out.println(deliveriesData.length);
                        playersOutByCaughtIn2010.put(batsman, playersOutByCaughtIn2010.get(batsman) + 1);
                    } else {
                        playersOutByCaughtIn2010.put(batsman, 1);
                    }
               }
           }
        }
        System.out.println("\nHow many time a particular player out by caught");
        System.out.println(playersOutByCaughtIn2010+"\n");
        return playersOutByCaughtIn2010;
    }

    static int findOverTakenByJjBumrahIn2016(List<Match> matches, List<Delivery> deliveries){
        List<Integer> matchIdsOfYear2016 = new ArrayList<Integer>() ;
        int matchId = 0;
        int matchSeason = 0;
        int deliveryId = 0;
        int overTakenByJJIn2010 = 0;
        String bowler = "";
        for(Match match: matches){
            matchId = match.getId();
            matchSeason = match.getSeason();
            if(!matchIdsOfYear2016.contains(matchId) && matchSeason==2016){
                matchIdsOfYear2016.add(matchId);
            }
        }
        int balls = 0;
        for ( Delivery delivery: deliveries ) {
            deliveryId = delivery.getMatchId();
            bowler = delivery.getBowler();
            if(matchIdsOfYear2016.contains(deliveryId) && bowler.equals("JJ Bumrah")  ){
                balls++;
            }
        }
        overTakenByJJIn2010 = balls/6;
        System.out.println("\nNumber of wickets taking by JJ Bumrah");
        System.out.println(overTakenByJJIn2010+"\n");
        return overTakenByJJIn2010;
    }

    static  HashMap<Integer,Integer> getNumberOfBoundaryHitByKohliIn2017(List<Match> matches, List<Delivery> deliveries){
        int matchId = 0;
        int matchSeason = 0;
        int deliveryId = 0;
        int batsmanRuns = 0;
        String batsman = "";
        List<Integer> matchIdsOfYear2017 = new ArrayList<Integer>() ;
        HashMap<Integer,Integer> numberOfBoundaryHit = new HashMap<>();
        for(Match match: matches){
            matchId = match.getId();
            matchSeason = match.getSeason();
            if(!matchIdsOfYear2017.contains(matchId) && matchSeason==2017){
                matchIdsOfYear2017.add(matchId);
            }
        }
        for ( Delivery delivery: deliveries) {
            deliveryId = delivery.getMatchId();
            batsman = delivery.getBatsman();
            batsmanRuns = delivery.getBatsmanRuns();
            if(matchIdsOfYear2017.contains(deliveryId) && batsman.equals("V Kohli") && batsmanRuns == 4  || batsmanRuns == 6){
                if(numberOfBoundaryHit.containsKey(batsmanRuns)){
                    numberOfBoundaryHit.put(batsmanRuns, numberOfBoundaryHit.get(batsmanRuns)+1);
                }else
                {
                    numberOfBoundaryHit.put(batsmanRuns,1);
                }
            }
        }
        System.out.println("\nNumber of boundary Hit by Kohli");
        System.out.println(numberOfBoundaryHit+"\n");
        return  numberOfBoundaryHit;
    }

    static HashMap<Integer,Integer> findRunScoredByChGayleInEachSeason(List<Match> matches, List<Delivery> deliveries){
        HashMap<Integer,Integer> matchesIdAndSeason= new HashMap<>();
        HashMap<Integer,Integer> chGayleRuns = new HashMap<>();
        int matchId = 0;
        int matchSeason = 0;
        int deliveryId = 0;
        int batsmanRuns = 0;
        int extraRuns = 0;
        String batsman = "";
        for(Match match: matches){
            matchId = match.getId();
            matchSeason = match.getSeason();
            matchesIdAndSeason.put(matchId,matchSeason);
        }
        for(Delivery delivery : deliveries){
            batsman = delivery.getBatsman();
            deliveryId = delivery.getMatchId();
            batsmanRuns = delivery.getBatsmanRuns();
            extraRuns = delivery.getExtraRuns();
            if(batsman.equals("CH Gayle")&& extraRuns == 0){
                if(chGayleRuns.containsKey(matchesIdAndSeason.get(deliveryId))) {
                    chGayleRuns.put(matchesIdAndSeason.get(deliveryId),chGayleRuns.get(matchesIdAndSeason.get(deliveryId))+(batsmanRuns));
                }
                else {
                    chGayleRuns.put(matchesIdAndSeason.get(deliveryId),batsmanRuns);
                }
            }
        }
        System.out.println("\nRuns scored by CH Gayle in each season");
        System.out.println(chGayleRuns+"\n");
        return chGayleRuns;
    }

    static List<String> findPlayersDismissedByDjHooda(List<Delivery> deliveries){
        List<String> playersDismissedByDjHooda= new ArrayList<>();
        String batsman ="";
        String bowler = "";
        String dismissalKind ="";
        for(Delivery delivery: deliveries){
          dismissalKind = delivery.getDismissalKind();
          bowler = delivery.getBowler();
          batsman = delivery.getBatsman();
            if(dismissalKind.equals("")){
                if(bowler.equals("DJ Hooda") ){
                    playersDismissedByDjHooda.add(batsman);
                }
            }
        }
        System.out.println("\nPlayer dismissed by DJ Hooda");
        System.out.println(playersDismissedByDjHooda+"\n");
        return playersDismissedByDjHooda;
    }

    static List<String> findPlayersWhoOutByStumped(List<Delivery> deliveries){
        List<String> playersWhoOutByStumped = new ArrayList<>();
        String batsman ="";
        String dismissalKind ="";
        for(Delivery delivery: deliveries)
        {
            dismissalKind = delivery.getDismissalKind();
            batsman = delivery.getBatsman();
            if(!dismissalKind.equals("")){
                if(dismissalKind.equals("stumped")){
                    playersWhoOutByStumped.add(batsman);
                }
            }
        }
        System.out.println("\nPlayer dismissed by stumped");
        System.out.println(playersWhoOutByStumped+"\n");
        return playersWhoOutByStumped;
    }
}