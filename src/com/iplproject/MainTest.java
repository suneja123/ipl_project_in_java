package com.iplproject;

import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    static Main main = new Main();
    static List<Match> matches = main.matchesData("src/matches.csv");
    static List<Delivery> deliveries = main.deliveriesData("src/deliveries.csv");;

    @Test
    void matchesData() {
        assertNotNull(main.matchesData("src/matches.csv"));
        assertEquals("ArrayList",(main.matchesData("src/matches.csv")).getClass().getSimpleName());
    }

    @Test
    void deliveriesData() {
        assertNotNull(main.deliveriesData("src/deliveries.csv"));
        assertEquals("ArrayList",(main.deliveriesData("src/deliveries.csv")).getClass().getSimpleName());
    }

    @Test
    void getMatchesPlayedPerYear() {
        Map<Integer,Integer> expectedMatchPlayedPerYear = new HashMap<>();
        Map<Integer, Integer> actualMatchesPlayedPerYear =  main.getMatchesPlayedPerYear(MainTest.matches);
        expectedMatchPlayedPerYear.put(2016,60);
        expectedMatchPlayedPerYear.put(2017,59);
        expectedMatchPlayedPerYear.put(2008,58);
        expectedMatchPlayedPerYear.put(2009,57);
        expectedMatchPlayedPerYear.put(2010,60);
        expectedMatchPlayedPerYear.put(2011,73);
        expectedMatchPlayedPerYear.put(2012,74);
        expectedMatchPlayedPerYear.put(2013,76);
        expectedMatchPlayedPerYear.put(2014,60);
        expectedMatchPlayedPerYear.put(2015,59);
        assertEquals(expectedMatchPlayedPerYear,actualMatchesPlayedPerYear);
        assertEquals(10,actualMatchesPlayedPerYear.size());
    }

    @Test
    void getMatchesWonPerTeamInPerYear() {
        Map<String, Integer> expectedNumberOfMatchesWonPerTeam = new HashMap<>();
        Map<String, Integer> actualNumberOfMatchesWonPerTeam =  Main.getMatchesWonPerTeamInPerYear(matches);
        expectedNumberOfMatchesWonPerTeam.put("Mumbai Indians", 92);
        expectedNumberOfMatchesWonPerTeam.put("Sunrisers Hyderabad", 42);
        expectedNumberOfMatchesWonPerTeam.put("Pune Warriors", 12);
        expectedNumberOfMatchesWonPerTeam.put("Rajasthan Royals", 63);
        expectedNumberOfMatchesWonPerTeam.put("Kolkata Knight Riders", 77);
        expectedNumberOfMatchesWonPerTeam.put("Royal Challengers Bangalore", 73);
        expectedNumberOfMatchesWonPerTeam.put("Gujarat Lions", 13);
        expectedNumberOfMatchesWonPerTeam.put("Rising Pune Supergiant", 10);
        expectedNumberOfMatchesWonPerTeam.put("Kochi Tuskers Kerala", 6);
        expectedNumberOfMatchesWonPerTeam.put("Kings XI Punjab", 70);
        expectedNumberOfMatchesWonPerTeam.put("Deccan Chargers", 29);
        expectedNumberOfMatchesWonPerTeam.put("Delhi Daredevils", 62);
        expectedNumberOfMatchesWonPerTeam.put("Rising Pune Supergiants", 5);
        expectedNumberOfMatchesWonPerTeam.put("Chennai Super Kings", 79);
        assertEquals(expectedNumberOfMatchesWonPerTeam, actualNumberOfMatchesWonPerTeam);
        assertEquals(14 ,main.getMatchesWonPerTeamInPerYear(MainTest.matches).size());
    }

    @Test
    void getExtraRunConcededPerTeamIn2016() {
        Map<String, Integer> expectedExtraRunsConcededByTeam = new HashMap<>();
        Map<String, Integer> actualExtraRunsConcededByTeam = main.getExtraRunsConcededPerTeam(matches, deliveries);
        expectedExtraRunsConcededByTeam.put("Gujarat Lions", 98);
        expectedExtraRunsConcededByTeam.put("Mumbai Indians", 102);
        expectedExtraRunsConcededByTeam.put("Sunrisers Hyderabad", 107);
        expectedExtraRunsConcededByTeam.put("Kings XI Punjab", 100);
        expectedExtraRunsConcededByTeam.put("Delhi Daredevils", 106);
        expectedExtraRunsConcededByTeam.put("Rising Pune Supergiants", 108);
        expectedExtraRunsConcededByTeam.put("Kolkata Knight Riders", 122);
        expectedExtraRunsConcededByTeam.put("Royal Challengers Bangalore", 156);
        assertEquals(expectedExtraRunsConcededByTeam, actualExtraRunsConcededByTeam);
        assertEquals(8, actualExtraRunsConcededByTeam.size());
    }

    @Test
    void getTopEconomicalBowlersIn2015() {
        int expectedTopEconomicalBowlersIn2015 = 99;
        int actualTopEconomicalBowlersIn2015 = main.getTopEconomicalBowlersIn2015(matches,deliveries).size();
        assertEquals(expectedTopEconomicalBowlersIn2015,actualTopEconomicalBowlersIn2015);
    }

    @Test
    void findCountHowManyTimesParticularPlayerOutByCaughtIn2010() {
        int expectedHowManyTimesParticularPlayerOutByCaughtIn2010 = 113;
        int actualHowManyTimesParticularPlayerOutByCaughtIn2010 = main.getCountHowManyTimesParticularPlayerOutByCaughtIn2010(matches,deliveries).size();
        assertEquals(expectedHowManyTimesParticularPlayerOutByCaughtIn2010,actualHowManyTimesParticularPlayerOutByCaughtIn2010);
    }

    @Test
    void findOverTakenByJjBumrahIn2016() {
        int expectedOverTakenByJjBumrahIn2016 = 53;
        int actualOverTakenByJjBumrahIn2016 = main.findOverTakenByJjBumrahIn2016(matches,deliveries);
        assertEquals(expectedOverTakenByJjBumrahIn2016,actualOverTakenByJjBumrahIn2016);
    }

    @Test
    void findNumberOfBoundaryHitByKohliIn2017() {
        Map<Integer,Integer> expctedNumberOfBoundaryHitByKohliIn2017 = main.getNumberOfBoundaryHitByKohliIn2017(matches,deliveries);
        Map<Integer,Integer> actualNumberOfBoundaryHitByKohliIn2017 = new HashMap<>();
        actualNumberOfBoundaryHitByKohliIn2017.put(4,23);
        actualNumberOfBoundaryHitByKohliIn2017.put(6,6523);
        assertEquals(expctedNumberOfBoundaryHitByKohliIn2017,actualNumberOfBoundaryHitByKohliIn2017);
        assertEquals(2,actualNumberOfBoundaryHitByKohliIn2017.size());
    }

    @Test
    void findRunScoredByChGayleInEachSeason() {
        int expectedRunScoredByChGayleInEachSeason = 9;
        int actualRunScoredByChGayleInEachSeason = main.findRunScoredByChGayleInEachSeason(matches,deliveries).size();
        assertEquals(expectedRunScoredByChGayleInEachSeason,actualRunScoredByChGayleInEachSeason);
    }

    @Test
    void findPlayersDismissedByDjHooda() {
        int expectedPlayersDismissedByDjHooda = 195;
        int actualPlayersDismissedByDjHooda = main.findPlayersDismissedByDjHooda(deliveries).size();
        assertEquals(expectedPlayersDismissedByDjHooda,actualPlayersDismissedByDjHooda);
    }

    @Test
    void findPlayersWhoOutByStumped() {
        int expectedPlayersWhoOutByStumped = 243;
        int actualPlayersWhoOutByStumped = main.findPlayersWhoOutByStumped(deliveries).size();
        assertEquals(expectedPlayersWhoOutByStumped,actualPlayersWhoOutByStumped);
    }
}