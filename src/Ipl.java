import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Ipl {
    public static void main(String[] args)  {
        String deliveriesDataPath = "/home/lenovo/Downloads/deliveries.csv";
        String matchesDataPath =  "/home/lenovo/Downloads/matches.csv";
        try {
            BufferedReader deliveriesBr=new BufferedReader(new FileReader(deliveriesDataPath));
            BufferedReader matchesBr=new BufferedReader(new FileReader(matchesDataPath));
            matchesPlayedPerYear(matchesBr);
            //matchesWonPerTeamInPerYear(matchesBr);
            //extraRunConcededPerTeamIn2016(matchesBr,deliveriesBr);
            //topEconomicaBowlersIn2015(matchesBr,deliveriesBr);
            // playersOutByCaughtIn2010(matchesBr, deliveriesBr);
            //overTakenByJJIn2016(matchesBr, deliveriesBr);
            //boundriesHitByKohliIn2017(matchesBr, deliveriesBr);
            //runScoredByChGayleInEachSeason(matchesBr, deliveriesBr);
            //playersDismissedByDjHooda(matchesBr, deliveriesBr);
           // playersWhoOutByStumped(matchesBr, deliveriesBr);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    static int  matchesPlayedPerYear(BufferedReader  matchesBr)  {
        try {
            int c = 0;
            Map<String,Integer> matchesPlayedPerYear=new HashMap<String,Integer>();
            String line="";
            while ( (line = matchesBr.readLine()) != null ) {
                if(c!= 0){
                    String[] matchData = line.split(",");
                    if(matchesPlayedPerYear.containsKey(matchData[1])){
                        matchesPlayedPerYear.put(matchData[1],(matchesPlayedPerYear.get(matchData[1])+1));
                    }
                    else {
                        matchesPlayedPerYear.put(matchData[1],1);
                    }
                }
                c=1;
            }
            System.out.println(matchesPlayedPerYear);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return 0;
    }
    static int matchesWonPerTeamInPerYear(BufferedReader matchesBr){
        try {
            int c = 0;
            Map<String,Integer> matchesWonPerTeamInPerYear=new HashMap<String,Integer>();
            String line="";
            while ( (line = matchesBr.readLine()) != null ) {

                String[] s = line.split(",");
                String winner  = s[10];

                if(matchesWonPerTeamInPerYear.containsKey(winner)){
                    matchesWonPerTeamInPerYear.put(winner,(matchesWonPerTeamInPerYear.get(winner)+1));
                }
                else {
                    matchesWonPerTeamInPerYear.put(winner,1);
                }


            }
            System.out.println(matchesWonPerTeamInPerYear);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return 0;
    }
    static int extraRunConcededPerTeamIn2016(BufferedReader matchesBr,BufferedReader deliveriesBr){
        try{
            Map<String, Integer> strikeRate = new HashMap<String, Integer>();
            String line = "";
            List<String> matchIdsOfYear2016 = new ArrayList<String>() ;
            while ( (line = matchesBr.readLine()) != null) {

                String[] matchData = line.split(",");
                if(!matchIdsOfYear2016.contains(matchData [0]) && matchData [1].equals(("2016"))){
                    matchIdsOfYear2016.add(matchData [0]);
                    //System.out.println(s[1]);
                }
            }

            while ( (line = deliveriesBr.readLine()) != null ) {

                String[] deliveriesData = line.split(",");
                if(matchIdsOfYear2016.contains(deliveriesData[0])){
                    if(strikeRate.containsKey(deliveriesData[3])){
                        strikeRate.put(deliveriesData[3],(strikeRate.get(deliveriesData[3]))+Integer.parseInt(deliveriesData[16]));
                       // System.out.println((strikeRate.get(s[3]))+Integer.parseInt(s[16]));
                    }
                    else
                    {
                        strikeRate.put( deliveriesData[3], Integer.parseInt(deliveriesData[16]));

                    }

                }
                //System.out.println(strikeRate);

            }
            System.out.println(strikeRate);
        }
        catch(IOException e){

        }
        return 0;
    }

    static int topEconomicaBowlersIn2015( BufferedReader matchesBr,BufferedReader deliveriesBr){

        try{

            String line = "";
            List<String> matchIdsOfYear2015 = new ArrayList<String>() ;
            while ( (line = matchesBr.readLine()) != null) {

                String[] matchData = line.split(",");
                if(!matchIdsOfYear2015.contains(matchData[0]) && matchData[1].equals(("2015"))){
                    matchIdsOfYear2015.add(matchData[0]);
                    //System.out.println(s[1]);
                }

            }
            Map<String, List<Integer>> toatalRunAndBalls = new HashMap<>();

            while ( (line = deliveriesBr.readLine()) != null) {

                String[] deliveriesData = line.split(",");
                if (matchIdsOfYear2015.contains(deliveriesData[0])) {
                    //System.out.println("contains"+(s[16]));
                    if(deliveriesData[16].equals("0")) {
                        // System.out.println(("equals"+s[16]));
                        if(toatalRunAndBalls.containsKey(deliveriesData[8])){
                            toatalRunAndBalls.get(deliveriesData[8]).set(0,toatalRunAndBalls.get(deliveriesData[8]).get(0)+ Integer.parseInt(deliveriesData[17]));
                            toatalRunAndBalls.get(deliveriesData[8]).set(1,toatalRunAndBalls.get(deliveriesData[8]).get(1)+ 1);

                        }
                        else
                        {
                            toatalRunAndBalls.put(deliveriesData[8],new ArrayList<Integer>());
                            toatalRunAndBalls.get(deliveriesData[8]).add(0,Integer.parseInt(deliveriesData[17]));
                            toatalRunAndBalls.get(deliveriesData[8]).add(1,1);
                        }
                    }

                }
            }
            //System.out.println(topEconomicalBowler);
            HashMap<String,Float> bowlersEconomi= new HashMap<String,Float>();

            for ( String key : toatalRunAndBalls.keySet() ) {
                int totalRun =  toatalRunAndBalls.get(key).get(0);
                int totalBall =  toatalRunAndBalls.get(key).get(1);
                float economi = totalRun/(totalBall/6f);

                //  System.out.println(totalRun+" "+ totalBall+" "+economi);

                bowlersEconomi.put(key,economi);
            }

            Map<String, Float> topEconomicalBowler = sortByValue(bowlersEconomi);

            System.out.println(topEconomicalBowler);
        }
        catch(IOException e){

        }


        return 0;
    }

    public static HashMap<String, Float> sortByValue(HashMap<String, Float> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Float> > list =
                new LinkedList<Map.Entry<String, Float> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, Float> >() {
            public int compare(Map.Entry<String, Float> o1,
                               Map.Entry<String, Float> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, Float> temp = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
    static  int playersOutByCaughtIn2010(BufferedReader matchesBr, BufferedReader deliveriesBr){
        try{
            Map<String, Integer> playersOutByCaughtIn2010 = new HashMap<String, Integer>();
            String line = "";
            List<String> matchIdsOfYear2010 = new ArrayList<String>() ;
            while ( (line = matchesBr.readLine()) != null ) {

                String[] matchData = line.split(",");
                if(!matchIdsOfYear2010.contains(matchData [0]) && matchData [1].equals(("2010"))){
                    matchIdsOfYear2010.add(matchData [0]);
                    //System.out.println(s[1]);
                }
            }
            while ( (line = deliveriesBr.readLine()) != null ) {
                String[] deliveriesData = line.split(",");

                if(matchIdsOfYear2010.contains(deliveriesData[0])&& deliveriesData.length == 21) {

                    if (deliveriesData[19].equals("caught")) {
                        //System.out.println(deliveriesData[6]);
                        if (playersOutByCaughtIn2010.containsKey(deliveriesData[6])) {
                            // System.out.println(deliveriesData.length);
                            playersOutByCaughtIn2010.put(deliveriesData[6], playersOutByCaughtIn2010.get(deliveriesData[6]) + 1);
                        } else {
                            playersOutByCaughtIn2010.put(deliveriesData[6], 1);
                        }
                    }

                }
            }
            System.out.println(playersOutByCaughtIn2010);
        }catch (IOException e){
            System.out.printf("e");
        }
        return 0;
    }
    static int overTakenByJJIn2016(BufferedReader matchesBr, BufferedReader deliveriesBr){
        try {
            String line = "";
            List<String> matchIdsOfYear2016 = new ArrayList<String>() ;
            while ( (line = matchesBr.readLine()) != null) {

                String[] matchData = line.split(",");
                if(!matchIdsOfYear2016.contains(matchData [0]) && matchData [1].equals(("2016"))){
                    matchIdsOfYear2016.add(matchData [0]);
                    //System.out.println(s[1]);
                }
            }
           // System.out.println(matchIdsOfYear2016);
            int balls = 0;
            while ( (line = deliveriesBr.readLine()) != null ) {
                String[] deliveriesData = line.split(",");


                if(matchIdsOfYear2016.contains(deliveriesData[0]) && deliveriesData[8].equals("JJ Bumrah")  ){
                    //System.out.println("hello");
                    balls++;
                }
            }
            int overTakenByJJIn2010 = balls/6;
            System.out.println("over "+overTakenByJJIn2010);
        }catch (IOException e){
            System.out.println(e);
        }

        return 0;
    }
    static int boundriesHitByKohliIn2017(BufferedReader matchesBr, BufferedReader deliveriesBr){
        try {
            String line = "";
            List<String> matchIdsOfYear2017 = new ArrayList<String>() ;
            while ( (line = matchesBr.readLine()) != null ) {

                String[] matchData = line.split(",");
                if(!matchIdsOfYear2017.contains(matchData [0]) && matchData [1].equals(("2016"))){
                    matchIdsOfYear2017.add(matchData [0]);
                    //System.out.println(s[1]);
                }
            }
            // System.out.println(matchIdsOfYear2016);
            HashMap<String,Integer> runs = new HashMap<>();
            while ( (line = deliveriesBr.readLine()) != null ) {
                String[] deliveriesData = line.split(",");


                if(matchIdsOfYear2017.contains(deliveriesData[0]) && deliveriesData[6].equals("V Kohli") && deliveriesData[15].equals("4") ||deliveriesData[15].equals("6")){
                    //System.out.println("hello");
                    if(runs.containsKey(deliveriesData[15])){
                        runs.put(deliveriesData[15], runs.get(deliveriesData[15])+1);
                    }else
                    {
                        runs.put(deliveriesData[15],1);
                    }
                }
            }

            System.out.println("boundaryCount "+runs);
        }catch (IOException e){
            System.out.println(e);
        }
        return 0;
    }
    static int runScoredByChGayleInEachSeason(BufferedReader matchesBr, BufferedReader deliveriesBr){

        HashMap<String,String> matchesIdAndSeason= new HashMap<>();
        String line = "";
        try {
            while((line = matchesBr.readLine()) != null){

                String[] matchData = line.split(",");
                matchesIdAndSeason.put(matchData[0],matchData[1]);
            }
            HashMap<String,Integer> chGayleRuns = new HashMap<>();
           // System.out.println(matchesIdAndSeason);
            while ((line = deliveriesBr.readLine())!=null){
                String[] deliveriesData = line.split(",");
                if(deliveriesData[6].equals("CH Gayle")&& deliveriesData[16].equals("0")){
                    if(chGayleRuns.containsKey(matchesIdAndSeason.get(deliveriesData[0])))
                    {
                        chGayleRuns.put(matchesIdAndSeason.get(deliveriesData[0]),chGayleRuns.get(matchesIdAndSeason.get(deliveriesData[0]))+Integer.parseInt(deliveriesData[15]));
                    }
                    else
                    {
                        chGayleRuns.put(matchesIdAndSeason.get(deliveriesData[0]),Integer.parseInt(deliveriesData[15]));
                    }
                }
            }
            System.out.println(chGayleRuns);

        }catch (IOException e){

        }

        return 0;
    }
    static int playersDismissedByDjHooda(BufferedReader matchesBr, BufferedReader deliveriesBr){
        List<String> playersDismissedByDjHooda= new ArrayList<>();
        String line = "";
        try {
            while((line = deliveriesBr.readLine())!=null){
                String[] deliveriesData = line.split(",");
                if(deliveriesData.length == 21){
                  //  System.out.println(deliveriesData[8]+" "+deliveriesData[19]);
                    if(deliveriesData[8].equals("DJ Hooda") ){
                       // System.out.println(deliveriesData[8]+" "+deliveriesData[19]);
                        playersDismissedByDjHooda.add(deliveriesData[6]);
                    }
                }
            }
            System.out.println(playersDismissedByDjHooda);

        }catch (IOException e){
            System.out.println(e);
        }

        return 0;
    }
    static int playersWhoOutByStumped(BufferedReader matchesBr, BufferedReader deliveriesBr){

        try {
            List<String> playersWhoOutByStumped = new ArrayList<>();
            String line = "";
            while ((line = deliveriesBr.readLine())!= null){
                String[] deliveriesData = line.split(",");
                if(deliveriesData.length == 21){
                    //System.out.println(deliveriesData[19]);
                    if(deliveriesData[19].equals("stumped")){
                        playersWhoOutByStumped.add(deliveriesData[6]);
                    }
                }
            }
            System.out.println(playersWhoOutByStumped);

        }catch (IOException e){
            System.out.println(e);
        }
        return 0;
    }

}









































































































































































































































































































































































































































































































































































